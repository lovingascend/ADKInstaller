English|[中文](Readme.md)

# ADKInstaller<a name="EN-US_TOPIC_0235601568"></a>

The ADKInstaller tool runs on an Ubuntu, x86 environment. It is used to set up the PC environment and board environment for the Atlas 200 DK.

**Note: The ADK version that this tool is adapted to is version 1.3x, that is, the installation tool when the 200DK operating environment is ubuntu16.04. And the current version of Shengteng official website is no longer maintained, and the warehouse is no longer maintained. For more information, please refer to Ascend's official document https://www.hiascend.com/document and Ascend's official sample repository is https://gitee.com/ascend/samples.**

## Environment Setup Overview<a name="en-us_topic_0233432619_section137245294533"></a>

The following figure shows the functions of ADKInstaller and the process of setting up the environment.

**Figure  1**  Environment setup flowchart<a name="en-us_topic_0233432619_fig61281288284"></a>  
![](figures/environment-setup-flowchart.png "environment-setup-flowchart")

>![](public_sys-resources/icon-note.gif) **NOTE:**   
>-   If it is your first time to use Atlas 200 DK, you need to learn the basics about the Atlas 200 DK and prepare the environment.  
>-   If a prepared SD card is included in your Atlas 200 DK, you can skip the SD card making step.  

## Preparing for Environment Setup<a name="en-us_topic_0233432619_section8534138124114"></a>

1.  Learn about the Atlas 200 DK.

    Huawei Atlas 200 Developer Kit \(Atlas 200 DK for short\) is a developer board product based on the Huawei Ascend 310 processor. It enables one-stop development of AI applications.

    The following figure shows the system block diagram of the Atlas 200 DK.

    **Figure  2**  Connection between the Atlas 200 DK developer board and  Mind Studio<a name="en-us_topic_0233432619_fig179514213618"></a>  
    ![](figures/connection-between-the-atlas-200-dk-developer-board-and-mind-studio.png "connection-between-the-atlas-200-dk-developer-board-and-mind-studio")

    >![](public_sys-resources/icon-note.gif) **NOTE:**   
    >The IP address 192.168.1.2/192.168.0.2 in the figure is the IP address of the developer board, which can be selected during SD card making.  

    The Atlas 200 DK developer board consists of the Hi3559 camera module and Atlas 200 AI accelerator module. The PC installed with the IDE Mind Studio connects to the Atlas 200 DK developer board over the USB port or Ethernet port.

    Mind Studio provides the device development kit \(DDK\) and a range of tool modules for model management, operator and model building, and log analysis. The DDK contains the library files required for building on the device side, as well as the tools, dependent libraries, and common header files required on the user's development machine.

2.  Prepare the environment.

    Before starting ADKInstaller, prepare the hardware and environment as follows:

    1\). Prepare the Atlas 200 DK, including an SD card for creating the Atlas 200 DK system boot disk, an SD card reader, a Type-C cable used for connecting to the Ubuntu server, and a camera.

    2\). Install the Ubuntu 16.04 OS on the server for deploying Mind Studio.

    3\). Check if at least 20 GB is available on the Ubuntu OS.

    4\). Check if the Ubuntu system memory is at least 4 GB.


## Setting up the Environment<a name="en-us_topic_0233432619_section845024719402"></a>

1.  Obtain ADKInstaller.

    Log in to the Ubuntu OS, switch to the common user \(user who installs the Atlas 200 DK development environment\), and run the following command in the command line to download the ADKInstaller software package:

    **wget  [https://obs-book.obs.cn-east-2.myhuaweicloud.com/temp/ADKInstaller.0.0.1.tar.gz](https://obs-book.obs.cn-east-2.myhuaweicloud.com/temp/ADKInstaller.0.0.1.tar.gz)**

    Run the following commands to decompress the software package and go to the directory where the decompressed file is stored:

    **tar -xzvf ADKInstaller.0.0.1.tar.gz**

    **cd ADKInstaller**

    Run the following commands to switch to the  **root** user and grant the  **sudo** permission to the common user:

    **su root**

    **./add\_sudo.sh** _username_

    **exit**

    >![](public_sys-resources/icon-note.gif) **NOTE:**   
    >-   Replace  _**username**_ with the user name of the common user who installs the Atlas 200DK development environment, for example,  **ascend**.  
    >-   You need to grant the  **sudo** permission to the common user for environment setup. You can run the  **./del\_sudo.sh  _username_**  command as the  **root** user to remove its sudo permission afterwards.  

2.  Start ADKInstaller.

    During installation, please refer to FAQ for problems（ https://gitee.com/lovingascend/ADKInstaller/blob/master/FAQ.md  ）Solve it.

    Run the following command as the common user in the Ubuntu OS to start ADKInstaller:

    **./ADKInstaller**

    [Figure 3](#en-us_topic_0233432619_fig15272194819563)  shows the ADKInstaller login window.

    **Figure  3**  Login window<a name="en-us_topic_0233432619_fig15272194819563"></a>  
    ![](figures/login-window.png "login-window")

    Enter your user name and password to log in to ADKInstaller. The default language is English. You can change the language as required. If ADKInstaller is started by the  **root** user or an incorrect password is entered, you will not be able to proceed.

    [Figure 4](#en-us_topic_0233432619_fig83075490273)  shows the home page after successful login. ADKInstaller will walk you through the major steps of environment setup: SD card making, DK connection, and Mind Studio installation. You will start with  **STEP 01 Make SD Card**.

    **Figure  4**  ADKInstaller home page<a name="en-us_topic_0233432619_fig83075490273"></a>  
    ![](figures/adkinstaller-home-page.png "adkinstaller-home-page")

3.  Make an SD card.

    >![](public_sys-resources/icon-note.gif) **NOTE:**   
    >If you already have your SD card made, check if the developer board can be started properly using this SD card. If yes, click  **Skip** to skip this step.  

    1.  After login, on the  **STEP 01 Make SD Card**  page:

        Confirm the software copyright as shown in  [Figure 5](#en-us_topic_0233432619_fig2122418112812)  and click  **OK**  to proceed.

        **Figure  5**  Software copyright<a name="en-us_topic_0233432619_fig2122418112812"></a>  
        ![](figures/software-copyright.png "software-copyright")

    2.  Click  **Make SD** on the lower right as shown in  [Figure 6](#en-us_topic_0233432619_fig9827841133717). The environment dependencies will be automatically downloaded and installed.

        When you place the cursor on  <u>**SD INFO**</u>, you can view the IP addresses of the USB NIC and physical NIC of the target developer board. The default IP address of the USB NIC is 192.168.158.2, and that of the physical NIC is 192.168.157.2.

        **Figure  6**  Start making SD card<a name="en-us_topic_0233432619_fig9827841133717"></a>  
        ![](figures/start-making-sd-card.png "start-making-sd-card")

    3.  In the dialog box \([Figure 7](#en-us_topic_0233432619_fig1595221213352)\) displayed on the click of  **Make SD**, perform the following operations:

        **Figure  7**  Settings of SD card making<a name="en-us_topic_0233432619_fig1595221213352"></a>  
        ![](figures/settings-of-sd-card-making.png "settings-of-sd-card-making")

        1.  Connect the SD card reader with an SD card inserted to the PC, click  **Refresh**, and select the name of the SD card.
        2.  Select the USB NIC IP address \(default: 192.168.158.2\) and physical NIC IP address of the developer board. The physical NIC IP address is bound to the USB NIC IP address and will be automatically updated with the USB NIC IP address.
        3.  Click  **OK**  to start SD card making.

    4.  After SD card making is completed, a dialog box is displayed, as shown in  [Figure 8](#en-us_topic_0233432619_fig051214619365). Click  **OK**.

        **Figure  8**  SD card making completed<a name="en-us_topic_0233432619_fig051214619365"></a>  
        ![](figures/sd-card-making-completed.png "sd-card-making-completed")

4.  Connect to the Atlas 200 DK.

    >![](public_sys-resources/icon-note.gif) **NOTE:**   
    >If the Atlas 200 DK has been connected, click  **Skip** to skip this step.  

    1.  Insert the prepared SD card into the Atlas 200 DK.
    2.  Connect the PC and the Atlas 200 DK using a Type-C cable.
    3.  Power on the Atlas 200 DK. Wait until all the four LEDs on the board are on \(about 15 minutes\), indicating that the DK is started successfully.
    4.  Click  **STEP 02 Connect Atlas 200 DK**  on the left. Click  **Refresh** to refresh the network. The virtual NIC is automatically selected. If there are multiple DKs, select the target virtual NIC. Select the IP address of the Atlas 200 DK. Click  **Connect** to connect to the Atlas 200 DK.

        ![](figures/en-us_image_0233842263.png)

    If you have any questions about DK connection, click  **tips**  to view the detailed connection description.

5.  Install Mind Studio.

    1.  Click  **STEP 03 Setup Mind Studio**, as shown in  [Figure 9](#en-us_topic_0233432619_fig1943017118568). Confirm the Mind Studio copyright and click  **OK**.

        **Figure  9**  Mind Studio copyright<a name="en-us_topic_0233432619_fig1943017118568"></a>  
        ![](figures/mind-studio-copyright.png "mind-studio-copyright")

    2.  Click  **Setup** in the lower right, as shown in  [Figure 10](#en-us_topic_0233432619_fig13380547155712). The environment dependencies will be automatically downloaded and installed.

        **Figure  10**  Preparing for Mind Studio installation<a name="en-us_topic_0233432619_fig13380547155712"></a>  
        

        ![](figures/en-us_image_0233687204.png)

    3.  As shown in  [Figure 11](#en-us_topic_0233432619_fig591311510594), the environment is ready for Mind Studio installation. Click  **OK** to start to install Mind Studio.

        **Figure  11**  Mind Studio installation started<a name="en-us_topic_0233432619_fig591311510594"></a>  
        ![](figures/mind-studio-installation-started.png "mind-studio-installation-started")

    4.  After Mind Studio is installed, the message "Mind Studio install successfully" is displayed, as shown in  [Figure 12](#en-us_topic_0233432619_fig196509151219).

        **Figure  12**  Mind Studio installed successfully<a name="en-us_topic_0233432619_fig196509151219"></a>  
        ![](figures/mind-studio-installed-successfully.png "mind-studio-installed-successfully")

    5.  Click  **OK**, starting Mind Studio.

        The subsequent operations must be performed in Mind Studio. During Mind Studio startup, you need to confirm whether to import previous configurations.

    >![](public_sys-resources/icon-note.gif) **NOTE:**   
    >All operations with ADKInstaller are complete. You can run the **./del\_sudo.sh  _username_**  command as the  **root** user to remove the sudo permission.  

6.  Configure the DDK and developer board.
    1.  Configure the DDK.

        Configure the DDK by referring to the following link:

        [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200347960.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/en-us_topic_0200347960.html)

    2.  Configure the DK.

        Configure the DK by referring to the following link:

        [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200347922.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/en-us_topic_0200347922.html)

    3.  Upgrade the DK.

        Upgrade the DK as required by referring to the following link:

        [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200348044.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/en-us_topic_0200348044.html)

        You can also perform manual upgrade by referring to the following link \(in the case of an upgrade failure\):

        [https://www.huaweicloud.com/ascend/doc/Atlas200DK/1.31.0.0\(beta\)/zh/zh-cn\_topic\_0182634979.html](https://www.huaweicloud.com/ascend/doc/Atlas200DK/1.31.0.0(beta)/zh/en-us_topic_0182634979.html)

    4.  Synchronize the library files by referring to the following link:

        [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0201537270.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/en-us_topic_0201537270.html)



## Hands-on Your First AI Application<a name="en-us_topic_0233432619_section147911829155918"></a>

Build your first AI application by referring to the following link:

[https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200347819.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/zh-cn_topic_0200347819.html)

## Appendix<a name="en-us_topic_0233432619_section1676879104"></a>

-   **Running ADKInstaller in Source Code Mode**
    1.  Obtain the source code package.

        Download the code in the  [https://gitee.com/Atlas200DK/ADKInstaller](https://gitee.com/Atlas200DK/ADKInstaller)  repository to any directory of the Ubuntu OS, for example,  **$HOME/ADKInstaller**.

    2.  Prepare the environment.

        Install python3 and pyqt.

        **sudo pip3 install --upgrade pip**

        **sudo pip3 install PyQt5 -i  [http://mirrors.aliyun.com/pypi/simple/](http://mirrors.aliyun.com/pypi/simple/)    --trusted-host  [mirrors.aliyun.com](http://mirrors.aliyun.com/)**

        **sudo apt-get install qt5-default qttools5-dev-tools**

        **pip3 install paramiko**

        **pip3 install psutil**

    3.  Start ADKInstaller.

        Go to the code download directory, for example,  **$HOME/ADKInstaller**, and run the following command to start ADKInstaller:

        **python3  ADKInstaller.py**

        The login page is displayed.


-   **See Also**

    Atlas 200 DK documentation:

    [https://www.huaweicloud.com/ascend/doc/Atlas200DK/1.31.0.0\(beta\)/zh/zh-cn\_topic\_0188535624.html](https://www.huaweicloud.com/ascend/doc/Atlas200DK/1.31.0.0(beta)/zh/en-us_topic_0188535624.html)

    Mind Studio instructions:

    [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200347859.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/en-us_topic_0200347859.html)

    Atlas 200 DK Forum

    [https://bbs.huaweicloud.com/forum/forum-726-1.html](https://bbs.huaweicloud.com/forum/forum-726-1.html)


## FAQ

 &emsp;[https://gitee.com/lovingascend/ADKInstaller/blob/master/FAQ.md](https://gitee.com/lovingascend/ADKInstaller/blob/master/FAQ.md)

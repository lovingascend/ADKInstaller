<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>MainForm</name>
    <message>
        <location filename="../MainForm.py" line="136"/>
        <source>ADKInstaller0.0.1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../MainForm.py" line="137"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../MainForm.py" line="138"/>
        <source>STEP 01
Make 
SD card</source>
        <translation>第一步
制卡</translation>
    </message>
    <message>
        <location filename="../MainForm.py" line="139"/>
        <source>STEP 02
Connect 
Atlas 200 DK</source>
        <translation>第二步
连接Atlas 200 DK</translation>
    </message>
    <message>
        <location filename="../MainForm.py" line="140"/>
        <source>STEP 03
Setup 
MindStudio</source>
        <translation>第三步
安装
Mind Studio</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../DialogConnect200DK.py" line="75"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogConnect200DK.py" line="76"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../DialogConnect200DK.py" line="77"/>
        <source>Follow the steps above to connect the development board to the PC, and then click the Refresh Button.</source>
        <translation>参考上面的动图，连接开发板和Ubuntu服务器，然后点击刷新按钮</translation>
    </message>
    <message>
        <location filename="../DialogConnect200DK.py" line="78"/>
        <source>Select the Atlas 200 DK IP(usb0).</source>
        <translation>选择Atlas 200 DK 的USB IP</translation>
    </message>
    <message>
        <location filename="../DialogConnect200DK.py" line="79"/>
        <source>192.168.1.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogConnect200DK.py" line="80"/>
        <source>192.168.158.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogConnect200DK.py" line="81"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../DialogConnect200DK.py" line="83"/>
        <source>Skip</source>
        <translation>跳过</translation>
    </message>
    <message>
        <location filename="../DialogConnect200DK.py" line="82"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../DialogConnect200DK.py" line="84"/>
        <source>USB MODE</source>
        <translation>USB模式</translation>
    </message>
</context>
</TS>

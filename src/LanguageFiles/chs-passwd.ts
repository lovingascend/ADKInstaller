<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="GetPasswd.py" line="86"/>
        <source>Please enter the current user password</source>
        <translation>请输入当前用户密码</translation>
    </message>
    <message>
        <location filename="GetPasswd.py" line="87"/>
        <source>User</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="GetPasswd.py" line="88"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="GetPasswd.py" line="89"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="GetPasswd.py" line="90"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="GetPasswd.py" line="91"/>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
</context>
</TS>

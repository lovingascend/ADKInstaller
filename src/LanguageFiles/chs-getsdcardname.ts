<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="getSDCardName.py" line="138"/>
        <source>GetSDCardName</source>
        <translation>获取SD卡信息</translation>
    </message>
    <message>
        <location filename="getSDCardName.py" line="139"/>
        <source>Tips </source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="getSDCardName.py" line="140"/>
        <source>1. Please insert the SD card and click the refresh button, and select the sd card name.</source>
        <translation>1.请插入SD卡然后点击刷新按钮，之后选择SD卡的名称。</translation>
    </message>
    <message>
        <location filename="getSDCardName.py" line="141"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="getSDCardName.py" line="142"/>
        <source>2. Select Atlas 200 DK IP: </source>
        <translation>2.选择Atlas 200 DK的IP：</translation>
    </message>
</context>
</TS>

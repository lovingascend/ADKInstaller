    if [ -f "/etc/NetworkManager/NetworkManager.conf" ]; then
        sed -i "s/managed=false/managed=true/g" /etc/NetworkManager/NetworkManager.conf
        #ifconfig ${usb_ethernet} down 1>/dev/null 2>&1
        #ifconfig ${usb_ethernet} up 1>/dev/null 2>&1
        service NetworkManager restart
        echo "NetworkManager service restarted."
    else
        # ifconfig ${usb_ethernet} down 1>/dev/null 2>&1
        #service networking restart
        /etc/init.d/networking restart
        echo "NetworkManager service restarted."
    fi


from PyQt5.QtWidgets import *
from DialogConnect200DK import Ui_Dialog
from PyQt5.QtCore import *


class step2_connect200dk(QWidget, Ui_Dialog):
    def __init__(self, language=None, path=None):
        super(step2_connect200dk, self).__init__()
        self.language = language
        self.path_html = path
        self.setupUi(self, language=self.language, path=self.path_html)
        self.setMinimumWidth(30)
        self.setMinimumHeight(30)
        self.tab_ = self.init_ui_layout()
        self.bottom_step2 = self.init_bottom_step2()

    def init_bottom_step2(self):
        bnt_wid = QWidget()
        bnt_layout = QHBoxLayout()
        bnt_layout.addWidget(self.pushButton_connect)
        bnt_layout.addWidget(self.pushButton_connect_next)
        bnt_layout.setContentsMargins(0, 4, 0, 0)
        bnt_wid.setLayout(bnt_layout)
        bottom_step2 = QWidget()
        bottom_vwid = QWidget()
        if self.language == '简体中文':
            bottom_step2_layout = QHBoxLayout()
            bottom_step2_layout.addStretch(28)
            bottom_step2_layout.addWidget(bnt_wid)
            bottom_step2_layout.addSpacing(35)
            bottom_step2.setLayout(bottom_step2_layout)
            bottom_vlayout = QVBoxLayout()
            bottom_vlayout.addSpacing(14)
            bottom_vlayout.addWidget(bottom_step2)
            bottom_vlayout.addSpacing(65)
            bottom_vwid.setLayout(bottom_vlayout)
        else:
            bottom_step2_layout = QHBoxLayout()
            bottom_step2_layout.addStretch(28)
            bottom_step2_layout.addWidget(bnt_wid)
            bottom_step2_layout.addSpacing(35)
            bottom_step2.setLayout(bottom_step2_layout)
            bottom_vlayout = QVBoxLayout()
            bottom_vlayout.addSpacing(10)
            bottom_vlayout.addWidget(bottom_step2)
            bottom_vlayout.addSpacing(63)
            bottom_vwid.setLayout(bottom_vlayout)
        return bottom_vwid

    def init_ui_layout(self):
        step_widget = QWidget(self)
        connect_layout = QGridLayout()
        connect_layout.setContentsMargins(30, 0, 30, 0)
        connect_layout.addWidget(self.label_2, 1, 2, 4, 6)
        connect_layout.addWidget(self.label, 5, 0, 1, 7)
        connect_layout.addWidget(self.pushButton_refresh, 6, 0, 1, 1)
        connect_layout.addWidget(self.comboBox_virtual_net_card, 6, 2, 1, 2)
        connect_layout.addWidget(self.pushButton_tips, 6, 4, 1, 1)
        connect_layout.addWidget(self.label_4, 8, 0, 2, 6)
        connect_layout.addWidget(self.comboBox_default_ip, 9, 0, 2, 1)
        step_widget.setLayout(connect_layout)
        step_widget.resize(1050,690)
        self.tabWidget.addTab(step_widget, "USB MODE")
        return self.tabWidget


from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class CollapsibleBox(QtWidgets.QWidget):

    def __init__(self, title="", show_status=False, pip_source_show=False, apt_source_show=False,  comboBox=None, comboBox_apt=None, comboBox_tar=None, comboBox_iso=None, parent=None, resize_signal=None):
        super(CollapsibleBox, self).__init__(parent)
        self.title = title
        self.comboBox_pip2 = comboBox
        if comboBox_apt != None:
            self.comboBox_all = comboBox_apt
        elif comboBox_tar != None:
            self.comboBox_all = comboBox_tar
        elif comboBox_iso!= None:
            self.comboBox_all = comboBox_iso
        self.resize_signal = resize_signal  # type: pyqtSignal
        self.toggle_button = QtWidgets.QToolButton(text=title, checkable=True, checked=False)
        font = QtGui.QFont()
        font.setBold(True)
        font.setPointSize(12)
        font.setWeight(75)
        self.toggle_button.setFont(font)
        self.toggle_button.setStyleSheet("QToolButton { border: none; }")
        self.toggle_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.toggle_button.setArrowType(QtCore.Qt.RightArrow)
        self.toggle_button.pressed.connect(self.on_pressed)

        self.toggle_animation = QtCore.QParallelAnimationGroup(self)

        self.content_area = QtWidgets.QScrollArea(maximumHeight=0, minimumHeight=0)
        self.content_area.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        self.content_area.setFrameShape(QtWidgets.QFrame.NoFrame)

        content_area_layout = QtWidgets.QVBoxLayout(self)
        content_area_layout.setContentsMargins(0, 0, 0, 0)

        if show_status and apt_source_show:
            head_widget = QWidget()
            hLayout = QHBoxLayout()
            hLayout.addWidget(self.toggle_button)
            hLayout.addWidget(self.comboBox_all)
            hLayout.setContentsMargins(0, 0, 0, 0)
            head_widget.setLayout(hLayout)
            head_widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
            content_area_layout.addWidget(head_widget)

        elif pip_source_show:
            head_widget = QWidget()
            hLayout = QHBoxLayout()
            hLayout.addWidget(self.toggle_button)
            hLayout.addWidget(self.comboBox_pip2)
            #hLayout.addStretch(1)
            hLayout.setContentsMargins(0, 0, 0, 0)
            head_widget.setLayout(hLayout)
            head_widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
            content_area_layout.addWidget(head_widget)

        content_area_layout.addWidget(self.content_area)
        content_area_layout.setAlignment(QtCore.Qt.AlignTop)

        self.toggle_animation.addAnimation(QtCore.QPropertyAnimation(self, b"minimumHeight"))
        self.toggle_animation.addAnimation(QtCore.QPropertyAnimation(self, b"maximumHeight"))
        self.toggle_animation.addAnimation(QtCore.QPropertyAnimation(self.content_area, b"maximumHeight"))
        self.toggle_animation.addAnimation(QtCore.QPropertyAnimation(self.content_area, b"minimumHeight"))

    def resizeEvent(self, resizeEvent: QResizeEvent):
        self.resize_signal.emit(self.title, self.content_area.maximumSize())

    @QtCore.pyqtSlot()
    def on_pressed(self):
        checked = self.toggle_button.isChecked()
        self.toggle_button.setArrowType(
            QtCore.Qt.DownArrow if not checked else QtCore.Qt.RightArrow
        )
        self.toggle_animation.setDirection(
            QtCore.QAbstractAnimation.Forward
            if not checked
            else QtCore.QAbstractAnimation.Backward
        )
        self.toggle_animation.start()

    def setContentLayout(self, layout, expandingHeight):
        old_layout = self.content_area.layout()
        del old_layout
        self.content_area.setLayout(layout)

        collapsed_height = (self.sizeHint().height() - self.content_area.maximumHeight())

        for i in range(2):
            animation = self.toggle_animation.animationAt(i)
            animation.setDuration(300)
            animation.setStartValue(collapsed_height)
            animation.setEndValue(collapsed_height + expandingHeight)

        for i in range(2):
            content_animation = self.toggle_animation.animationAt(i + 2)
            content_animation.setDuration(300)
            content_animation.setStartValue(0)
            content_animation.setEndValue(expandingHeight)


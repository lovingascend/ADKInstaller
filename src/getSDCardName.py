# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'getSDCardName.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import sys
import os
import configparser
import re
from Common import ADKUtils

class Ui_Dialog(object):
    def setupUi(self, Dialog, language=None):
        Dialog.setObjectName("Dialog")
        Dialog.resize(768, 534)
        self.language = language
        self.trans_getsdname= QTranslator(Dialog)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        font = QtGui.QFont()
        font.setUnderline(True)
        self.label_2 = QtWidgets.QLabel(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setObjectName("label_2")
        self.label_2.setFont(font)
        self.label_2.setWordWrap(True)
        self.label_2.setToolTip("How to check sd card name: In the Terminal, use the fdisk -l command to view\n the changes in the list before and after inserting and removing the SD card, \nso as to determine the name of the device that needs to be made. \nIf the pop-up window asks you to format the SD card, please click cancel or ignore.")
        self.gridLayout.addWidget(self.label_2, 2, 2, 1, 1, Qt.AlignCenter | Qt.AlignVCenter)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonBox.sizePolicy().hasHeightForWidth())
        self.buttonBox.setSizePolicy(sizePolicy)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 10, 2, 1, 2)
        self.label_3 = QtWidgets.QLabel(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setObjectName("label_3")
        self.label_3.setWordWrap(True)
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 4)
        self.pushButton = QtWidgets.QPushButton(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setObjectName("pushButton")
        self.pushButton.setFixedSize(180, 30)
        self.gridLayout.addWidget(self.pushButton, 2, 0, 1, 1)
        self.comboBox = QtWidgets.QComboBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
        self.comboBox.setSizePolicy(sizePolicy)
        self.comboBox.setMaximumSize(QtCore.QSize(160, 127))
        self.comboBox.setCurrentText("")
        self.comboBox.setObjectName("comboBox")
        self.comboBox.setFixedSize(180, 30)
        self.gridLayout.addWidget(self.comboBox, 2, 1, 1, 1)

        self.step3_label = QtWidgets.QLabel()
        parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
        conf = configparser.ConfigParser()
        conf.read(parent_dir + "/config.ini")
        self.usb_label = QtWidgets.QLabel()
        self.usb_label.setText("usb IP")
        self.comboBox_default_usb_ip = QtWidgets.QComboBox()
        default_usb_ip1 = conf.get("make_sd_card_IP1", "default_usb_IP")
        default_usb_ip1_str = re.compile(r'\"(.*)\"')
        default_usb_ip1_str.findall(default_usb_ip1)
        default_usb_ip2 = conf.get("make_sd_card_IP2", "default_usb_IP")
        default_usb_ip1_str.findall(default_usb_ip2)
        self.comboBox_default_usb_ip.addItem(default_usb_ip1_str.findall(default_usb_ip1)[0])
        self.comboBox_default_usb_ip.addItem(default_usb_ip1_str.findall(default_usb_ip2)[0])
        self.eth0_label = QtWidgets.QLabel()
        self.eth0_label.setText("eth0 IP")
        self.comboBox_default_eth0_ip = QtWidgets.QComboBox()
        default_eth0_ip1 = conf.get("make_sd_card_IP1", "default_eth0_IP")
        default_usb_ip1_str.findall(default_eth0_ip1)
        default_eth0_ip2 = conf.get("make_sd_card_IP2", "default_eth0_IP")
        default_usb_ip1_str.findall(default_eth0_ip2)
        self.comboBox_default_eth0_ip.addItem(default_usb_ip1_str.findall(default_eth0_ip1)[0])
        self.comboBox_default_eth0_ip.addItem(default_usb_ip1_str.findall(default_eth0_ip2)[0])
        usb_wid = QWidget()
        usb_hlayout = QHBoxLayout()
        usb_hlayout.addWidget(self.usb_label)
        usb_hlayout.addWidget(self.comboBox_default_usb_ip)
        usb_wid.setLayout(usb_hlayout)
        eth_wid = QWidget()
        eth_hlayout = QHBoxLayout()
        eth_hlayout.addWidget(self.eth0_label)
        eth_hlayout.addWidget(self.comboBox_default_eth0_ip)
        eth_wid.setLayout(eth_hlayout)
        self.gridLayout.setContentsMargins(20, 20, 20, 20)
        self.gridLayout.addWidget(self.step3_label, 5, 0, 1, 1, Qt.AlignLeft | Qt.AlignVCenter)
        self.gridLayout.addWidget(usb_wid, 6, 0, 1, 1, Qt.AlignCenter | Qt.AlignTop)
        self.gridLayout.addWidget(eth_wid, 6, 1, 1, 1, Qt.AlignCenter | Qt.AlignTop)
        ADKUtils.disable_item_comboBox(self.comboBox_default_eth0_ip, [0, 1], 1 | 32)
        self.comboBox_default_usb_ip.currentIndexChanged.connect(self.changeIndex)

        if self.language == "简体中文":
            self.change_chs(Dialog)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def changeIndex(self):
        if self.comboBox_default_usb_ip.currentIndex() == 0:
            self.comboBox_default_eth0_ip.setCurrentIndex(0)
            ADKUtils.disable_item_comboBox(self.comboBox_default_eth0_ip, [0, 1], 0)
        if self.comboBox_default_usb_ip.currentIndex() == 1:
            self.comboBox_default_eth0_ip.setCurrentIndex(1)
            ADKUtils.disable_item_comboBox(self.comboBox_default_eth0_ip, [0, 1], 0)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "GetSDCardName"))
        self.label_2.setText(_translate("Dialog", "Tips "))
        self.label_3.setText(_translate("Dialog", "1. Please insert the SD card and click the refresh button, and select the sd card name."))
        self.pushButton.setText(_translate("Dialog", "Refresh"))
        self.step3_label.setText(_translate("Dialog", "2. Select Atlas 200 DK IP: "))

    def change_chs(self, MainWindow):
        self.project_path = os.path.dirname(os.path.abspath(sys.argv[0]))
        self.trans_getsdname.load(self.project_path + '/LanguageFiles/chs-getsdcardname')
        _app = QApplication.instance()
        _app.installTranslator(self.trans_getsdname)
        self.retranslateUi(MainWindow)

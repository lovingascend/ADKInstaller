from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from MindStudio import Ui_MainWindow_MS
from PyQt5.QtCore import *
import CollapsibleBox
import collections

class step3_mindstudio(QWidget, Ui_MainWindow_MS):
    resize_signal = pyqtSignal(str, QSize)

    def resize_trigger(self, name: str, qsize: QSize):
        if name == 'apt-get install':
            self.apt_size = qsize
        if name == 'pip install    ':
            self.pip_size = qsize
        if name == 'pip3 install   ':
            self.pip3_size = qsize
        if name == 'download:      ':
            self.download_size = qsize
        self.component_widget.resize(830, self.apt_size.height() + self.pip_size.height() + self.pip3_size.height() + self.download_size.height() + 200)


    def __init__(self, language=None):
        super(step3_mindstudio, self).__init__()
        self.language = language
        self.setupUi(self, language=self.language)
        self.setMinimumWidth(30)
        self.setMinimumHeight(30)
        self.download_list = ["mindstudio", "ddk"]
        self.download_dict = collections.OrderedDict()
        self.download_dict["mindstudio"] = {
                "url": self.mindstudio_url,
                "progressBar": self.progressBar_7,
                "checkBox": self.checkBox_7,
                "statusLabel": self.ubuntu_arm_label,
                "download_finished": False,
                }
        self.download_dict["ddk"] = {
                "url": self.ddk_url,
                "progressBar": self.progressBar_6,
                "checkBox": self.checkBox_8,
                "statusLabel": self.mini_label,
                "download_finished": False,
                }

        for key in self.download_dict:
            self.download_dict[key]["progressBar"].setValue(0)
            self.download_dict[key]["statusLabel"].setText("")
            self.download_dict[key]["checkBox"].setDisabled(True)

        self.apt_install_list = ["xterm", "python", "python3", "python-pip", "python3-pip", "openjdk-8-jdk", "fonts-droid-fallback", "ttf-wqy-zenhei", "ttf-wqy-microhei", "fonts-arphic-ukai", "fonts-arphic-uming"]

        self.apt_install_dict = collections.OrderedDict()
        self.apt_install_dict["apt-get update"] = {"command": "apt-get update -y",
                "progressBar": self.progressBar_apt_update,
                "checkBox":self.checkBox_apt_update,
                "statusLabel": self.apt_update_label,
                "isInstalled": False}
        self.apt_install_dict["xterm"] = {"command": "apt-get install ",
                "progressBar": self.progressBar_xunbuntu_desktop,
                "checkBox": self.checkBox_xubuntu_desktop,
                "statusLabel": self.xubuntu_desktop_label,
                "isInstalled": False}
        self.apt_install_dict["python"] = {"command": "apt-get install ",
                "progressBar": self.progressBar_python,
                "checkBox": self.checkBox_python,
                "statusLabel": self.python_label,
                "isInstalled": False}
        self.apt_install_dict["python3"] = {"command": "apt-get install ",
                "progressBar": self.progressBar_python3,
                "checkBox": self.checkBox_python3,
                "statusLabel": self.python3_label,
                "isInstalled": False}
        self.apt_install_dict["python-pip"] = {"command": "apt-get install ",
                "progressBar": self.progressBar_python_pip,
                "checkBox": self.checkBox_python_pip,
                "statusLabel": self.python_pip_label,
                "isInstalled": False}
        self.apt_install_dict["python3-pip"] = {"command": "apt-get install ",
                "progressBar": self.progressBar_python3_pip,
                "checkBox": self.checkBox_python3_pip,
                "statusLabel": self.python3_pip_label,
                "isInstalled": False}
        self.apt_install_dict["openjdk-8-jdk"] = {"command": "apt-get install ",
                "progressBar": self.progressBar_openjdk,
                "checkBox": self.checkBox_openjdk,
                "statusLabel": self.openjdk_label,
                "isInstalled": False}

        self.apt_install_dict["fonts-droid-fallback"] = {"command": "apt-get install ",
                                                  "progressBar": self.progressBar_fonts_droid_fallback,
                                                  "checkBox": self.checkBox_fonts_droid_fallback,
                                                  "statusLabel": self.label_fonts_droid_fallback,
                                                  "isInstalled": False}
        self.apt_install_dict["ttf-wqy-zenhei"] = {"command": "apt-get install ",
                                                  "progressBar": self.progressBar_ttf_wqy_zenhei,
                                                  "checkBox": self.checkBox_ttf_wqy_zenhei,
                                                  "statusLabel": self.label_ttf_wqy_zenhei,
                                                  "isInstalled": False}
        self.apt_install_dict["ttf-wqy-microhei"] = {"command": "apt-get install ",
                                                  "progressBar": self.progressBar_ttf_wqy_microhei,
                                                  "checkBox": self.checkBox_ttf_wqy_microhei,
                                                  "statusLabel": self.label_ttf_wqy_microhei,
                                                  "isInstalled": False}
        self.apt_install_dict["fonts-arphic-ukai"] = {"command": "apt-get install ",
                                                  "progressBar": self.progressBar_fonts_arphic_ukai,
                                                  "checkBox": self.checkBox_fonts_arphic_ukai,
                                                  "statusLabel": self.label_fonts_arphic_ukai,
                                                  "isInstalled": False}
        self.apt_install_dict["fonts-arphic-uming"] = {"command": "apt-get install ",
                                                  "progressBar": self.progressBar_fonts_arphic_uming,
                                                  "checkBox": self.checkBox_fonts_arphic_uming,
                                                  "statusLabel": self.label_fonts_arphic_uming,
                                                  "isInstalled": False}
        for key in self.apt_install_dict:
            self.apt_install_dict[key]["progressBar"].setValue(0)
            self.apt_install_dict[key]["statusLabel"].setText("")
            self.apt_install_dict[key]["checkBox"].setDisabled(True)

        self.apt_update_label.setText("")
        self.progressBar_apt_update.setValue(0)

        self.pip2_install_list = [["pip2_upgrade", "pip install --upgrade pip"],
                                  ["numpy", "pip2 install numpy==1.14 --user"],
                                  ["decorator", "pip2 install decorator --user"]]

        self.pip2_install_dict = collections.OrderedDict()
        self.pip2_install_dict["pip2_upgrade"] = {
                "progressBar": self.progressBar_pip2_upgrade,
                "checkBox": self.checkBox_pip2_upgrade,
                "statusLabel": self.pip2_upgrade_label,
                "isInstalled": False}
        self.pip2_install_dict["numpy"] = {
                "progressBar": self.progressBar_numpy_pip,
                "checkBox": self.checkBox_pip_numpy,
                "statusLabel": self.numpy_pip_label,
                "isInstalled": False}
        self.pip2_install_dict["decorator"] = {"progressBar": self.progressBar_decorator_pip,
                "checkBox": self.checkBox_pip_decorator,
                "statusLabel": self.decorator_pip_label,
                "isInstalled": False}

        self.pip3_install_list = [["pip3_upgrade", "pip3 install --upgrade pip"],
                                  ["numpy", "pip3 install numpy==1.14 --user"],
                                  ["decorator", "pip3 install decorator --user"]]
        self.pip3_install_dict = collections.OrderedDict()
        self.pip3_install_dict["pip3_upgrade"] = {
                "progressBar": self.progressBar_pip3_upgrade,
                "checkBox": self.checkBox_pip3_upgrade,
                "statusLabel": self.pip3_upgrade_label,
                "isInstalled": False}
        self.pip3_install_dict["numpy"] = {
                "progressBar": self.progressBar_numpy_pip3,
                "checkBox": self.checkBox_pip3_numpy,
                "statusLabel": self.numpy_pip3_label,
                "isInstalled": False}
        self.pip3_install_dict["decorator"] = {
                "progressBar": self.progressBar_decorator_pip3,
                "checkBox": self.checkBox_pip3_decorator,
                "statusLabel": self.decorator_pip3_label,
                "isInstalled": False}

        self.checkBox_pip_numpy.setDisabled(True)
        self.checkBox_pip_decorator.setDisabled(True)
        self.checkBox_pip3_numpy.setDisabled(True)
        self.checkBox_pip3_decorator.setDisabled(True)

        for key in self.pip2_install_dict:
            self.pip2_install_dict[key]["progressBar"].setValue(0)
            self.pip2_install_dict[key]["statusLabel"].setText("")

        for key in self.pip3_install_dict:
            self.pip3_install_dict[key]["progressBar"].setValue(0)
            self.pip3_install_dict[key]["statusLabel"].setText("")

        self.pushButton_ms_install.setEnabled(False)
        self.pushButton_ms_install.setVisible(False)
        self.tab_ = self.init_ui_layout()
        self.bottom_step3_wid = self.init_bottom_widget()

    def init_components_window_widget(self):
        components_window_scroll = QScrollArea()
        self.apt_size = QSize(0, 0)
        self.pip_size = QSize(0, 0)
        self.pip3_size = QSize(0, 0)
        self.download_size = QSize(0, 0)


        self.resize_signal.connect(self.resize_trigger)

        apt_content_area_widget = QWidget()
        self.collapsible_box_layout_initial(apt_content_area_widget, self.apt_install_dict)
        apt_content_area_wrapper_layout = QVBoxLayout()
        apt_content_area_wrapper_layout.addWidget(apt_content_area_widget)
        apt_content_area_wrapper_layout.setContentsMargins(0, 0, 0, 0)
        apt_collapsible_box = CollapsibleBox.CollapsibleBox("apt-get install", True, False, True, comboBox_apt=self.comboBox_apt, resize_signal=self.resize_signal)
        apt_collapsible_box.setContentLayout(apt_content_area_wrapper_layout, len(self.apt_install_dict) * 40)

        pip_content_area_widget = QWidget()
        self.collapsible_box_layout_initial(pip_content_area_widget, self.pip2_install_dict)
        pip_content_area_wrapper_layout = QVBoxLayout()
        pip_content_area_wrapper_layout.addWidget(pip_content_area_widget)
        pip_content_area_wrapper_layout.setContentsMargins(0, 0, 0, 0)
        pip_collapsible_box = CollapsibleBox.CollapsibleBox("pip install    ",False, True, False, comboBox=self.comboBox_pip2, resize_signal=self.resize_signal)
        pip_collapsible_box.setContentLayout(pip_content_area_wrapper_layout, len(self.pip2_install_dict) * 40)

        pip3_content_area_widget = QWidget()
        self.collapsible_box_layout_initial(pip3_content_area_widget, self.pip3_install_dict)
        pip3_content_area_wrapper_layout = QVBoxLayout()
        pip3_content_area_wrapper_layout.addWidget(pip3_content_area_widget)
        pip3_content_area_wrapper_layout.setContentsMargins(0, 0, 0, 0)
        pip3_collapsible_box = CollapsibleBox.CollapsibleBox("pip3 install   ",False, True, False, comboBox=self.comboBox_pip3, resize_signal=self.resize_signal)
        pip3_collapsible_box.setContentLayout(pip3_content_area_wrapper_layout, len(self.pip3_install_dict) * 40)

        download_content_area_widget = QWidget()
        self.collapsible_box_layout_initial(download_content_area_widget, self.download_dict)
        download_content_area_wrapper_layout = QVBoxLayout()
        download_content_area_wrapper_layout.addWidget(download_content_area_widget)
        download_content_area_wrapper_layout.setContentsMargins(0, 0, 0, 0)
        download_collapsible_box = CollapsibleBox.CollapsibleBox("download:      ", True, False, True, comboBox_tar=self.comboBox_tar, resize_signal=self.resize_signal)
        download_collapsible_box.setContentLayout(download_content_area_wrapper_layout, len(self.download_dict) * 40)

        components_layout = QVBoxLayout()
        components_layout.addWidget(apt_collapsible_box)
        components_layout.addWidget(pip_collapsible_box)
        components_layout.addWidget(pip3_collapsible_box)
        components_layout.addWidget(download_collapsible_box)
        components_layout.setSpacing(10)
        components_layout.setAlignment(QtCore.Qt.AlignTop)

        components_widget = QWidget()
        components_widget.resize(830, 850)
        components_widget.setLayout(components_layout)
        self.component_widget = components_widget

        components_window_scroll.setWidget(components_widget)

        components_window_widget = QWidget()
        components_window_layout = QVBoxLayout()
        components_window_layout.addWidget(components_window_scroll)
        components_window_layout.setSpacing(0)
        components_window_widget.setLayout(components_window_layout)

        return components_window_widget
    def init_bottom_widget(self):
        download_path_widget = QWidget()
        download_path_layout = QHBoxLayout()
        download_path_layout.addSpacing(260)
        download_path_layout.addWidget(self.label_3)
        download_path_layout.addSpacing(0)
        download_path_layout.addWidget(self.lineEdit_download_path)
        download_path_widget.setLayout(download_path_layout)

        agreement_wid = QWidget()
        agreement_hlayout = QHBoxLayout()
        agreement_hlayout.addSpacing(261)
        agreement_hlayout.addWidget(self.checkBox_aggrement)
        agreement_wid.setLayout(agreement_hlayout)

        bottom_left_widget = QWidget()
        bottom_left_layout = QVBoxLayout()
        bottom_left_layout.addWidget(download_path_widget, 1)
        bottom_left_layout.addWidget(agreement_wid, 1)
        bottom_left_widget.setLayout(bottom_left_layout)

        bottom_btn_widget = QWidget()
        bottom_btn_layout = QHBoxLayout()
        bottom_btn_layout.addWidget(bottom_left_widget, 1)
        bottom_btn_layout.addWidget(self.pushButton)
        bottom_btn_layout.addWidget(self.pushButton_1)
        bottom_btn_layout.addWidget(self.pushButton_pause)
        bottom_btn_layout.addWidget(self.pushButton_ms_install)
        bottom_btn_layout.addSpacing(35)
        bottom_btn_widget.setLayout(bottom_btn_layout)

        progressbar_wid = QWidget()
        progressbar_wid_layout = QHBoxLayout()
        progressbar_wid_layout.addSpacing(260)
        progressbar_wid_layout.addWidget(self.progressBar_ms_install)
        progressbar_wid_layout.addSpacing(35)
        progressbar_wid.setLayout(progressbar_wid_layout)

        bottom_widget = QWidget()
        bottom_layout = QVBoxLayout()
        bottom_layout.addWidget(bottom_btn_widget)
        bottom_layout.addWidget(progressbar_wid)
        bottom_widget.setLayout(bottom_layout)

        return bottom_widget

    def collapsible_box_layout_initial(self, widget: QWidget, dict):
        index = 0
        for key in dict:
            item_widget = QWidget(widget)
            grid_layout = QGridLayout()
            grid_layout.addWidget(dict[key]["checkBox"], 0, 0, 1, 5)
            grid_layout.addWidget(dict[key]["progressBar"], 0, 5, 1, 4)
            grid_layout.addWidget(dict[key]["statusLabel"], 0, 9, 1, 3, Qt.AlignCenter)
            item_widget.setLayout(grid_layout)
            item_widget.setFixedSize(800, 40)
            item_widget.move(10, index * 40)
            index += 1
        widget.setMinimumSize(800, 40 * index)

    def init_ui_layout(self):
        components_window_widget = self.init_components_window_widget()
        make_sd_god_widget = QWidget()
        main_window_widget_layout = QVBoxLayout()
        main_window_widget_layout.addWidget(components_window_widget, 7)
        main_window_widget_layout.addSpacing(20)
        make_sd_god_widget.setLayout(main_window_widget_layout)
        self.tabWidget.addTab(make_sd_god_widget, "DETAIL")
        self.tabWidget.addTab(self.textBrowser_log, "LOG")
        self.tabWidget.resize(1050,690)
        return self.tabWidget


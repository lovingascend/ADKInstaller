from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from step1 import Ui_MainWindow
from PyQt5.QtCore import *
import CollapsibleBox
import collections
class step1_make_sd_card(QWidget, Ui_MainWindow):
    resize_signal = pyqtSignal(str, QSize)

    def resize_trigger(self, name: str, qsize: QSize):
        if name == 'Host components:':
            self.host_size = qsize
        if name == 'Target components:':
            self.target_size = qsize
        if name == 'Ubuntu components:':
            self.iso_size = qsize
        self.component_widget.resize(830, self.host_size.height() + self.target_size.height() + self.iso_size.height() + 200)

    def __init__(self, language=None):
        super(step1_make_sd_card, self).__init__()
        self.language = language
        self.setupUi(self, language=self.language)
        self.setMinimumWidth(30)
        self.setMinimumHeight(30)

        self.download_dict = collections.OrderedDict()
        self.download_dict["run_package"] = {
                "url": self.developerkit_version_url,
                "progressBar": self.progressBar_6,
                "checkBox": self.checkBox_7,
                "statusLabel": self.mini_label,
                "download_finished": False,
                }

        self.iso_dict = collections.OrderedDict()
        self.iso_dict["ubuntu_arm"] = {
            "url": "https://mirrors.huaweicloud.com/ubuntu-cdimage/releases/16.04.6/release/ubuntu-16.04.6-server-arm64.iso",
            "progressBar": self.progressBar_7,
            "checkBox": self.checkBox_8,
            "statusLabel": self.ubuntu_arm_label,
            "download_finished": False,
        }
        self.apt_install_list = ["qemu-user-static", "python3-yaml", "gcc-aarch64-linux-gnu",
                                                              "g++-aarch64-linux-gnu"]

        self.apt_install_dict = collections.OrderedDict()
        self.apt_install_dict["update"] = {"command": "apt-get update -y",
                "progressBar": self.progressBar_apt_update,
                "checkBox":self.checkBox_apt_update,
                "statusLabel": self.apt_update_label,
                "isInstalled": False}
        self.apt_install_dict["qemu-user-static"] = {"command": "apt-get install qemu-user-static -y",
                "progressBar": self.progressBar,
                "checkBox": self.checkBox,
                "statusLabel": self.qemu_label,
                "isInstalled": False}
        self.apt_install_dict["python3-yaml"] = {"command": "apt-get install python3-yaml -y",
                "progressBar": self.progressBar_3,
                "checkBox": self.checkBox_3,
                "statusLabel": self.yaml_label,
                "isInstalled": False}
        self.apt_install_dict["gcc-aarch64-linux-gnu"] = {"command": "apt-get install gcc-aarch64-linux-gnu -y",
                "progressBar": self.progressBar_4,
                "checkBox": self.checkBox_4,
                "statusLabel": self.gcc_label,
                "isInstalled": False}
        self.apt_install_dict["g++-aarch64-linux-gnu"] = {"command": "apt-get install g++-aarch64-linux-gnu -y",
                "progressBar": self.progressBar_5,
                "checkBox": self.checkBox_5,
                "statusLabel": self.gplusplus_label,
                "isInstalled": False}
        for key in self.download_dict:
            self.download_dict[key]["progressBar"].setValue(0)
            self.download_dict[key]["statusLabel"].setText("")
        for key in self.iso_dict:
            self.iso_dict[key]["progressBar"].setValue(0)
            self.iso_dict[key]["statusLabel"].setText("")
        for key in self.apt_install_dict:
            self.apt_install_dict[key]["progressBar"].setValue(0)
            self.apt_install_dict[key]["statusLabel"].setText("")

        self.apt_update_label.setText("")
        self.progressBar_apt_update.setValue(0)

        self.flashButton.setEnabled(True)
        self.tab_ = self.init_ui_layout()
        self.bottom = self.init_bottom_widget()

        self.pushButton.setEnabled(True)
        self.pushButton.setVisible(True)

        self.pushButton_pause.setEnabled(False)
        self.pushButton_pause.setVisible(False)

    def init_components_window_widget(self):
        components_window_scroll = QScrollArea()
        self.host_size = QSize(0, 0)
        self.target_size = QSize(0, 0)
        self.iso_size = QSize(0, 0)

        self.resize_signal.connect(self.resize_trigger)

        host_content_area_widget = QWidget()
        self.collapsible_box_layout_initial(host_content_area_widget, self.apt_install_dict)
        host_content_area_wrapper_layout = QVBoxLayout()
        host_content_area_wrapper_layout.addWidget(host_content_area_widget)
        host_content_area_wrapper_layout.setContentsMargins(0, 0, 0, 0)
        host_collapsible_box = CollapsibleBox.CollapsibleBox("Host components:", True, False, True, comboBox_apt=self.comboBox_apt, resize_signal=self.resize_signal)
        host_collapsible_box.setContentLayout(host_content_area_wrapper_layout, len(self.apt_install_dict) * 50)

        target_content_area_widget = QWidget()
        self.collapsible_box_layout_initial(target_content_area_widget, self.download_dict)
        target_content_area_wrapper_layout = QVBoxLayout()
        target_content_area_wrapper_layout.addWidget(target_content_area_widget)
        target_content_area_wrapper_layout.setContentsMargins(0, 0, 0, 0)
        target_collapsible_box = CollapsibleBox.CollapsibleBox("Target components:", True, False, True, comboBox_tar=self.comboBox_tar,resize_signal=self.resize_signal)
        target_collapsible_box.setContentLayout(target_content_area_wrapper_layout, len(self.download_dict) * 50)

        iso_content_area_widget = QWidget()
        self.collapsible_box_layout_initial(iso_content_area_widget, self.iso_dict)
        iso_content_area_wrapper_layout = QVBoxLayout()
        iso_content_area_wrapper_layout.addWidget(iso_content_area_widget)
        iso_content_area_wrapper_layout.setContentsMargins(0, 0, 0, 0)
        iso_collapsible_box = CollapsibleBox.CollapsibleBox("Ubuntu components:", True, False, True, comboBox_iso=self.comboBox_iso,resize_signal=self.resize_signal)
        iso_collapsible_box.setContentLayout(iso_content_area_wrapper_layout, len(self.iso_dict) * 50)

        components_layout = QVBoxLayout()
        components_layout.addWidget(host_collapsible_box)
        components_layout.addWidget(target_collapsible_box)
        components_layout.addWidget(iso_collapsible_box)
        components_layout.setSpacing(10)
        components_layout.setAlignment(QtCore.Qt.AlignTop)

        components_widget = QWidget()
        components_widget.resize(830, 540)
        components_widget.setLayout(components_layout)
        self.component_widget = components_widget

        components_window_scroll.setWidget(components_widget)

        components_window_widget = QWidget()
        components_window_layout = QVBoxLayout()
        components_window_layout.addWidget(components_window_scroll)
        components_window_layout.setSpacing(0)
        components_window_widget.setLayout(components_window_layout)

        return components_window_widget

    def collapsible_box_layout_initial(self, widget: QWidget, dict):
        index = 0
        for key in dict:
            item_widget = QWidget(widget)
            grid_layout = QGridLayout()
            grid_layout.addWidget(dict[key]["checkBox"], 0, 0, 1, 5)
            dict[key]["checkBox"].setDisabled(True)
            grid_layout.addWidget(dict[key]["progressBar"], 0, 5, 1, 4)
            grid_layout.addWidget(dict[key]["statusLabel"], 0, 9, 1, 3, Qt.AlignCenter)
            item_widget.setLayout(grid_layout)
            item_widget.setFixedSize(800, 50)
            item_widget.move(10, index * 50)
            index += 1
        widget.setMinimumSize(800, 50 * index)

    def init_bottom_widget(self):

        self.make_sd_progress = QWidget()
        self.make_sd_progress_layout = QHBoxLayout()
        self.make_sd_progress_layout.addSpacing(260)
        self.make_sd_progress_layout.addWidget(self.label_3)
        self.make_sd_progress_layout.addWidget(self.lineEdit_download_path)
        # self.make_sd_progress_layout.addSpacing(50)
        # self.make_sd_progress_layout.addWidget(self.label_makesd_info)
        self.make_sd_progress_layout.addSpacing(0)
        self.make_sd_progress.setLayout(self.make_sd_progress_layout)
        agreement_wid = QWidget()
        agreement_hlayout = QHBoxLayout()
        agreement_hlayout.addSpacing(261)
        agreement_hlayout.addWidget(self.checkBox_aggrement)
        agreement_hlayout.addStretch(1)
        agreement_hlayout.addWidget(self.label_makesd_info)
        agreement_wid.setLayout(agreement_hlayout)
        bottom_left_widget = QWidget()
        bottom_left_layout = QVBoxLayout()
        bottom_left_layout.addWidget(self.make_sd_progress, 1)
        bottom_left_layout.addWidget(agreement_wid, 1)
        bottom_left_widget.setLayout(bottom_left_layout)

        bottom_widget = QWidget()
        bottom_layout = QHBoxLayout()
        bottom_layout.addWidget(bottom_left_widget, 3)
        bottom_layout.addWidget(self.pushButton, 1)
        bottom_layout.addWidget(self.pushButton_pause, 1)
        bottom_layout.addWidget(self.flashButton, 1)
        bottom_layout.addSpacing(35)
        bottom_widget.setLayout(bottom_layout)
        progressbar_wid = QWidget()
        progressbar_wid_layout = QHBoxLayout()
        progressbar_wid_layout.addSpacing(260)
        progressbar_wid_layout.addWidget(self.progressBar_make_sd)
        progressbar_wid_layout.addSpacing(35)
        progressbar_wid.setLayout(progressbar_wid_layout)
        bottom_wid = QWidget()
        bottom_wid_layout = QVBoxLayout()
        bottom_wid_layout.addWidget(bottom_widget)
        bottom_wid_layout.addWidget(progressbar_wid)
        bottom_wid.setLayout(bottom_wid_layout)

        return bottom_wid

    def init_make_sd_progress(self):
        make_sd_progress = QWidget()
        make_sd_progress_layout = QHBoxLayout()
        make_sd_progress_layout.addSpacing(20)
        make_sd_progress_layout.addWidget(self.label_3, 1)
        make_sd_progress_layout.setSpacing(0)
        make_sd_progress_layout.addWidget(self.lineEdit_download_path, 3)
        make_sd_progress_layout.addStretch(1)
        make_sd_progress.setLayout(make_sd_progress_layout)
        return make_sd_progress

    def init_ui_layout(self):
        components_window_widget = self.init_components_window_widget()
        # bottom_widget = self.init_bottom_widget()
        # make_sd_progress = self.init_make_sd_progress()
        make_sd_god_widget = QWidget()
        main_window_widget_layout = QVBoxLayout()
        main_window_widget_layout.addWidget(components_window_widget, 3)
        # main_window_widget_layout.addWidget(make_sd_progress, 1)
        # main_window_widget_layout.addWidget(bottom_widget, 1)
        # main_window_widget_layout.addWidget(self.progressBar_make_sd, 1)
        main_window_widget_layout.addSpacing(20)
        make_sd_god_widget.setLayout(main_window_widget_layout)
        self.tabWidget.addTab(make_sd_god_widget, "DETAIL")
        self.tabWidget.addTab(self.textBrowser_log, "LOG")
        self.tabWidget.resize(1050,690)
        return self.tabWidget


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DialogConnect200DK.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import os
import sys


class Ui_Dialog(object):
    def setupUi(self, Dialog, language=None, path=None):
        self.language = language
        self.path_html = path
        self.trans_step2 = QTranslator(Dialog)
        Dialog.setObjectName("Dialog")
        Dialog.resize(1463, 941)
        self.comboBox_virtual_net_card = QtWidgets.QComboBox(Dialog)
        self.comboBox_virtual_net_card.setGeometry(QtCore.QRect(830, 380, 231, 51))
        self.comboBox_virtual_net_card.setObjectName("comboBox_virtual_net_card")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setFixedWidth(480)
        self.label_2.setFixedHeight(270)
        self.label_2.setObjectName("label_2")
        movie = QMovie("./connectAtlas200DK.gif")
        self.label_2.setMovie(movie)
        movie.start()
        self.pushButton_connect = QtWidgets.QPushButton(Dialog)
        self.pushButton_connect.setGeometry(QtCore.QRect(830, 810, 231, 61))
        self.pushButton_connect.setObjectName("pushButton_connect")
        self.pushButton_connect.setFixedSize(180, 80)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(180, 120, 1161, 29))
        self.label.setObjectName("label")
        self.label.setWordWrap(True)
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(190, 550, 571, 71))
        self.label_4.setObjectName("label_4")
        self.label_4.setWordWrap(True)
        self.comboBox_default_ip = QtWidgets.QComboBox(Dialog)
        self.comboBox_default_ip.setGeometry(QtCore.QRect(830, 560, 241, 51))
        self.comboBox_default_ip.setObjectName("comboBox_default_ip")
        self.comboBox_default_ip.addItem("")
        self.comboBox_default_ip.addItem("")
        self.pushButton_tips = QtWidgets.QPushButton(Dialog)
        self.pushButton_tips.clicked.connect(self.openUrl)
        self.pushButton_refresh = QtWidgets.QPushButton(Dialog)
        self.pushButton_refresh.setGeometry(QtCore.QRect(820, 180, 231, 61))
        self.pushButton_refresh.setObjectName("pushButton_refresh")
        self.pushButton_connect_next = QtWidgets.QPushButton(Dialog)
        self.pushButton_connect_next.setFixedSize(180, 80)
        self.pushButton_connect_next.setEnabled(True)
        self.tabWidget = QtWidgets.QTabWidget(Dialog)
        self.tabWidget.setObjectName("tabWidget")
        self.tabWidget.setCurrentIndex(0)
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")

        if self.language == "简体中文":
            self.change_chs(Dialog)
        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def openUrl(self):
        dir = self.path_html + "/html/connectDK_tips.html"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(dir))

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButton_connect.setText(_translate("Dialog", "Connect"))
        self.label.setText(_translate("Dialog", "Follow the steps above to connect the development board to the PC, and then click the Refresh Button."))
        self.label_4.setText(_translate("Dialog", "Select the Atlas 200 DK IP(usb0)."))
        self.comboBox_default_ip.setItemText(0, _translate("Dialog", "192.168.1.2"))
        self.comboBox_default_ip.setItemText(1, _translate("Dialog", "192.168.158.2"))
        self.pushButton_refresh.setText(_translate("Dialog", "Refresh"))
        self.pushButton_tips.setText(_translate("Dialog", "tips"))
        self.pushButton_connect_next.setText(_translate("Dialog", "Skip"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "USB MODE"))

    def change_chs(self, MainWindow):
        self.project_path = os.path.dirname(os.path.abspath(sys.argv[0]))
        self.trans_step2.load(self.project_path + '/LanguageFiles/chs-step2')
        _app = QApplication.instance()
        _app.installTranslator(self.trans_step2)
        self.retranslateUi(MainWindow)

    def change_english(self, MainWindow):
        _app = QApplication.instance()
        _app.removeTranslator(self.trans_step2)
        self.retranslateUi(MainWindow)

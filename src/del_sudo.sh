#!/bin/bash

CURRENT_USER=$1
CONFIG_FILE="/etc/sudoers.d/${CURRENT_USER}_specific"

if [ -f "${CONFIG_FILE}" ];then
    rm ${CONFIG_FILE}
fi

echo "delete sudo right for ${CURRENT_USER} successfully"


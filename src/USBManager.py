import psutil
import sys
import os
import time

import subprocess
from PyQt5.QtWidgets import *
import DialogResult
import re


class USBManager(object):
    def __init__(self):
        pass
    @classmethod
    def GetUsbName(cls):
        device_list=[]
        device_all = psutil.disk_partitions()
        if device_all==None or len(device_all) == 0:
            return None
        # print(device_all)
        for device in device_all:
            if device.mountpoint.find('/media') >=0 :
                device_list.append(device.device)
        result_list = []
        for device in device_list:
            device_temp = device[0:len(device)-1]
            if device_temp in result_list:
                continue
            result_list.append(device_temp)
        # print("all device list: ", result_list)
        return  result_list

    @classmethod
    def GetUsbName_by_fdisk(self, sudo_passwd):
        # device_list = []
        self.passwd = sudo_passwd

        command_str = 'echo ' + self.passwd +  ' | sudo -S fdisk -l |  grep /dev/sd | grep Disk  | awk -F: \'{print $1}\' | awk \'{print $2}\' '

        CompletedProcessObject = subprocess.run(args=command_str, shell=True, stdin=subprocess.PIPE,
                                                stdout=subprocess.PIPE,
                                                stderr=subprocess.PIPE, universal_newlines=True, timeout=10,
                                                check=False)
        if CompletedProcessObject:
            code, out, err = CompletedProcessObject.returncode, CompletedProcessObject.stdout, CompletedProcessObject.stderr

            if code == 0:
                if out:
                    device_list = out.split("\n")
                    # print("device_list = ", device_list)
                    command_find_sys = 'df -h | awk -F\' \' \'{ if($6 == "/" || $6 == "/boot") print $1}\' '
                    (ret, output) = subprocess.getstatusoutput(command_find_sys)
                    if ret == 0:
                        device_sys = output
                        for item in device_list:
                            if item == None:
                                device_list.remove(item)
                            if item == '':
                                device_list.remove(item)
                        for item in device_list:
                            item_name = re.compile(item)
                            result = item_name.findall(device_sys)
                            if result != []:
                                device_list.remove(item)
                                return device_list
                        return device_list
                if err:
                    return []
            else:
                if code == 1:
                    print("excute fdisk -l error")
                    return []
                else:
                    print("excute fdisk -l error")
                    return []
        else:
            return []

if __name__ == "__main__":
    USBManager.GetUsbName()
    USBManager.GetUsbName_by_fdisk("ascend")

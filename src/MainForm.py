# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainForm.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtCore import *
import step1_make_sd_card
import step2_connect200dk
import step3_mindstudio
import os
import sys
import configparser


class mywidget(QtWidgets.QWidget):
    def __init__(self, MainForm, a, b, c):
        super().__init__(MainForm)
        self.step1_make_sd_card = a
        self.step2_connect200dk = b
        self.step3_mindstudio = c

    def resizeEvent(self, evt):
        ne = evt.size()
        w = ne.width() - 335
        h = ne.height() - 270
        self.step1_make_sd_card.resize(w, h)
        self.step2_connect200dk.resize(w, h)
        self.step3_mindstudio.resize(w, h)


class mywidget_chs(QtWidgets.QWidget):
    def __init__(self, MainForm, a, b, c):
        super().__init__(MainForm)
        self.step1_make_sd_card = a
        self.step2_connect200dk = b
        self.step3_mindstudio = c

    def resizeEvent(self, evt):
        ne = evt.size()
        w = ne.width() - 335
        h = ne.height() - 280
        self.step1_make_sd_card.resize(w, h)
        self.step2_connect200dk.resize(w, h)
        self.step3_mindstudio.resize(w, h)


class Ui_MainForm(object):
    def setupUi(self, MainForm, language=None):
        self.language = language
        self.parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
        self.trans_mainform = QTranslator(MainForm)
        MainForm.setObjectName("MainForm")
        screen = QDesktopWidget().screenGeometry()
        MainForm.resize(screen.width() * 0.7, screen.height() * 0.7)
        MainForm.move((screen.width() * 0.3) / 2, (screen.height() * 0.3) / 2)

        self.step1_make_sd_card = step1_make_sd_card.step1_make_sd_card(language=self.language)
        self.step2_connect200dk = step2_connect200dk.step2_connect200dk(language=self.language, path=self.parent_dir)
        self.step3_mindstudio = step3_mindstudio.step3_mindstudio(language=self.language)

        if language == '简体中文':
            self.centralwidget = mywidget_chs(MainForm,
                                          self.step1_make_sd_card.tab_,
                                          self.step2_connect200dk.tab_,
                                          self.step3_mindstudio.tab_)
        else:
            self.centralwidget = mywidget(MainForm,
                                          self.step1_make_sd_card.tab_,
                                          self.step2_connect200dk.tab_,
                                          self.step3_mindstudio.tab_)
        mainform_x = self.centralwidget.x()
        mainform_y = self.centralwidget.y()
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame()
        self.frame.setMaximumSize(QtCore.QSize(200, 16777215))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.frame.setStyleSheet("QFrame{border: 0px;}")

        self.pushButton_help = QtWidgets.QPushButton(self.frame)
        self.pushButton_help.setFixedSize(180, 100)
        # self.pushButton_help.move(mainform_x, mainform_y)
        self.pushButton_makesd = QtWidgets.QPushButton(self.frame)
        self.pushButton_makesd.setFixedSize(180, 100)
        self.pushButton_makesd.setObjectName("pushButton_makesd")
        self.pushButton_makesd.setCheckable(True)
        self.pushButton_makesd.setChecked(True)
        self.pushButton_makesd.setAutoExclusive(True)
        # self.pushButton_makesd.move(mainform_x, mainform_y + 120)
        self.pushButton_connect = QtWidgets.QPushButton(self.frame)
        self.pushButton_connect.setObjectName("pushButton_connect")
        self.pushButton_connect.setFixedSize(180, 100)
        # self.pushButton_connect.move(mainform_x, mainform_y + 240)
        self.pushButton_ms_studio = QtWidgets.QPushButton(self.frame)
        self.pushButton_ms_studio.setObjectName("pushButton_ms_studio")
        self.pushButton_ms_studio.setFixedSize(180, 100)
        # self.pushButton_ms_studio.move(mainform_x, mainform_y + 360)
        self.button_layout = QVBoxLayout()
        self.button_layout.addStretch(0)
        self.button_layout.addWidget(self.pushButton_help)
        self.button_layout.addStretch(1)
        self.button_layout.addWidget(self.pushButton_makesd)
        self.button_layout.addStretch(1)
        self.button_layout.addWidget(self.pushButton_connect)
        self.button_layout.addStretch(1)
        self.button_layout.addWidget(self.pushButton_ms_studio)
        self.button_layout.addStretch(0)
        self.frame.setLayout(self.button_layout)

        MainForm.setCentralWidget(self.centralwidget)

        self.menubar = QtWidgets.QMenuBar(MainForm)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 23))
        self.menubar.setObjectName("menubar")
        MainForm.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainForm)
        self.statusbar.setObjectName("statusbar")
        MainForm.setStatusBar(self.statusbar)

        self.step2_connect200dk.setHidden(True)
        self.step3_mindstudio.setHidden(True)
        self.main_widget = QWidget()
        self.main_layout = QHBoxLayout()
        self.main_layout.addWidget(self.frame)
        self.main_layout.addSpacing(10)
        self.main_layout.addWidget(self.step1_make_sd_card)
        self.main_layout.addWidget(self.step2_connect200dk)
        self.main_layout.addWidget(self.step3_mindstudio)
        self.main_layout.setContentsMargins(50, 50, 50, 0)
        self.main_widget.setLayout(self.main_layout)
        self.bottom_step1_layout = QVBoxLayout()
        self.bottom_step1_layout.addWidget(self.main_widget, 5)
        self.bottom_step1_layout.addWidget(self.step1_make_sd_card.bottom)
        self.bottom_step1_layout.addWidget(self.step2_connect200dk.bottom_step2)
        self.bottom_step1_layout.addWidget(self.step3_mindstudio.bottom_step3_wid)
        self.step2_connect200dk.bottom_step2.setHidden(True)
        self.step3_mindstudio.bottom_step3_wid.setHidden(True)

        self.centralwidget.setLayout(self.bottom_step1_layout)

        self.pushButton_help.clicked.connect(self.openUrl)
        self.pushButton_makesd.clicked.connect(lambda: self.change(self.pushButton_makesd.objectName()))
        self.pushButton_connect.clicked.connect(lambda: self.change(self.pushButton_connect.objectName()))
        self.pushButton_ms_studio.clicked.connect(lambda: self.change(self.pushButton_ms_studio.objectName()))
        if self.language == "简体中文":
            self.change_chs(MainForm)
        self.retranslateUi(MainForm)
        QtCore.QMetaObject.connectSlotsByName(MainForm)

    def openUrl(self):
        if self.language == "简体中文":
            dir = self.parent_dir + "/html/Readme.html"
        else:
            dir = self.parent_dir + "/html/Readme_EN.html"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(dir))

    def retranslateUi(self, MainForm):
        _translate = QtCore.QCoreApplication.translate
        MainForm.setWindowTitle(_translate("MainForm", "ADKInstaller 0.0.1"))
        self.pushButton_help.setText(_translate("MainForm", "Help"))
        self.pushButton_makesd.setText(_translate("MainForm", "STEP 01\nMake \nSD card"))
        self.pushButton_connect.setText(_translate("MainForm", "STEP 02\nConnect \nAtlas 200 DK"))
        self.pushButton_ms_studio.setText(_translate("MainForm", "STEP 03\nSetup \nMindStudio"))

    def change(self, name):
        if name == "pushButton_makesd":
            self.step2_connect200dk.setHidden(True)
            self.step3_mindstudio.setHidden(True)
            self.step1_make_sd_card.setHidden(False)
            self.step2_connect200dk.bottom_step2.setHidden(True)
            self.step3_mindstudio.bottom_step3_wid.setHidden(True)
            self.step1_make_sd_card.bottom.setHidden(False)
            self.pushButton_makesd.setCheckable(True)
            self.pushButton_makesd.setChecked(True)
            self.pushButton_makesd.setAutoExclusive(True)
            self.pushButton_connect.setCheckable(True)
            self.pushButton_ms_studio.setCheckable(True)

        if name == "pushButton_connect":
            self.step1_make_sd_card.setHidden(True)
            self.step1_make_sd_card.bottom.setHidden(True)
            self.step3_mindstudio.setHidden(True)
            self.step3_mindstudio.bottom_step3_wid.setHidden(True)
            self.step2_connect200dk.setHidden(False)
            self.step2_connect200dk.bottom_step2.setHidden(False)
            conf = configparser.ConfigParser()
            conf.read(self.parent_dir + "/config.ini")
            default_usb_ip1 = conf.get("make_sd_info", "usb_ip")
            cur_usb_ip = self.step2_connect200dk.comboBox_default_ip.currentText()
            if not cur_usb_ip == default_usb_ip1:
                temp_ip = self.step2_connect200dk.comboBox_default_ip.currentText()
                self.step2_connect200dk.comboBox_default_ip.setItemText(0, self.step2_connect200dk.comboBox_default_ip.itemText(1))
                self.step2_connect200dk.comboBox_default_ip.setItemText(1, temp_ip)
            self.pushButton_makesd.setCheckable(False)
            self.pushButton_connect.setCheckable(True)
            self.pushButton_connect.setChecked(True)
            self.pushButton_connect.setAutoExclusive(True)
            self.pushButton_ms_studio.setCheckable(False)
        if name == "pushButton_ms_studio":
            self.step2_connect200dk.setHidden(True)
            self.step1_make_sd_card.setHidden(True)
            self.step1_make_sd_card.bottom.setHidden(True)
            self.step2_connect200dk.bottom_step2.setHidden(True)
            self.step3_mindstudio.setHidden(False)
            self.step3_mindstudio.bottom_step3_wid.setHidden(False)
            self.pushButton_makesd.setCheckable(False)
            self.pushButton_connect.setCheckable(False)
            self.pushButton_ms_studio.setCheckable(True)
            self.pushButton_ms_studio.setChecked(True)
            self.pushButton_ms_studio.setAutoExclusive(True)

    def change_chs(self, MainForm):
        self.project_path = os.path.dirname(os.path.abspath(sys.argv[0]))
        self.trans_mainform.load(self.project_path + '/LanguageFiles/chs-mainform')
        _app = QApplication.instance()
        _app.installTranslator(self.trans_mainform)
        self.retranslateUi(MainForm)

    def change_english(self, MainForm):
        _app = QApplication.instance()
        _app.removeTranslator(self.trans_mainform)
        self.retranslateUi(MainForm)

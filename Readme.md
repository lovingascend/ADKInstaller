中文|[English](Readme_EN.md)
# ADKInstaller<a name="ZH-CN_TOPIC_0233432619"></a>

本工具ADKInstaller运行在X86机器的Ubuntu环境上，实现Atlas200 DK PC端和单板端的环境搭建。

**注意：本工具适配的ADK版本为1.3x版本，即200DK的运行环境为ubuntu16.04时的安装工具。并且当前昇腾官网已经不维护这个版本了，本仓库也已经不维护了。更多信息见昇腾官方文档 [https://www.hiascend.com/document](https://www.hiascend.com/document) ，昇腾官方样例仓库为[https://gitee.com/ascend/samples](https://gitee.com/ascend/samples) 。**

## 环境搭建流程<a name="section137245294533"></a>

根据下图可以了解ADKInstaller工具的功能以及工具进行环境搭建的总体流程。

**图 1**  环境搭建流程图<a name="fig61281288284"></a>  
![](figures/环境搭建流程图.png "环境搭建流程图")

>![](public_sys-resources/icon-note.gif) **说明：**   
>-   首次使用者需要了解Atlas200DK，并做好环境准备工作。  
>-   如果你拿到的套件中包含了制作好的SD卡，可选择跳过制卡环境或重新制卡。  

## 环境搭建准备<a name="section8534138124114"></a>

1.  了解Atlas200 DK。

    华为Atlas 200 DK开发者套件 Atlas 200 Developer Kit（缩写：Atlas 200 DK ）是以华为Ascend 310芯片为核心的一个开发者板形态产品，为开发者提供一站式开发套件，助力开发者快速进行AI应用程序的开发。

    **图 2**  Atlas 200 DK系统框图<a name="fig930143542717"></a>  
    ![](figures/Atlas-200-DK系统框图.jpg "Atlas-200-DK系统框图")

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >-   图中的IP 192.168.1.2/192.168.0.2是默认的开发板的IP，在制卡阶段可以进行选择。  
    >-   如果你拿到的套件中包含了制作好的SD卡，可选择跳过制卡环境或重新制卡 。  

    Atlas 200 DK系统框图如图2-1所示，其中Atlas 200 DK开发者板主要包含Hi3559 Camera模块以及Atlas 200 AI加速模块，开发工具Mind Studio所在PC通过USB接口或者网线与Atlas 200 DK开发者板连接。

    开发工具 Mind Studio 包含了设备开发套件（Device Development Kit，DDK）以及各个工具模块（如模型管理工具、编译工具、日志工具等），其中DDK提供了设备侧编译时所依赖的库文件，用户开发机器使用的工具、依赖库以及公共头文件等。

2.  环境准备。

    启动ADKInstaller工具前，需要准备好以下设备和环境：

    1、准备一套 Atlas200DK 套件，包含制作Atlas 200 DK启动系统的SD卡,读卡器，与UI Host相连接的Type-C连接线及摄像头等配件。

    **2、已完成X86设备中Ubuntu16.04的环境安装。**

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >-   当前仅支持ubuntu系统的语言为英文。

    3、Ubuntu系统空余空间超过20G。

    4、Ubuntu系统内存大于4G。


## 环境搭建步骤<a name="section845024719402"></a>

1.  获取ADKInstaller。

    登录你的ubuntu系统，并切换到普通用户下（安装Atlas200DK的开发环境的用户） 的命令行下执行以下命令下载ADKInstaller。

    **wget  [https://obs-book.obs.cn-east-2.myhuaweicloud.com/temp/ADKInstaller.0.0.1.tar.gz](https://obs-book.obs.cn-east-2.myhuaweicloud.com/temp/ADKInstaller.0.0.1.tar.gz)**

    执行以下命令，解压软件压缩包并进入解压文件目录下。

    **tar -xzvf ADKInstaller.0.0.1.tar.gz**

    **cd ADKInstaller**

    执行以下命令，切换至root用户下为当前用户开启sudo权限。

    **su root**

    **./add\_sudo.sh** _username_

    **exit**

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >-   username需要替换为安装Atlas200DK的开发环境的普通用户的用户名，如ascend。  
    >-   这里需要临时给当前用户开启sudo权限，当安装完环境后可以在root用户下执行  **./del\_sudo.sh  _username_**  删除当前用户的sudo权限。  

2.  启动ADKInstaller

    安装过程中，出现的问题可以参考FAQ（ https://gitee.com/lovingascend/ADKInstaller/blob/master/FAQ.md ）进行解决。

    执行以下命令，启动ADKInstaller。

    **./ADKInstaller**

    启动ADKInstaller后如[图3](#fig15272194819563)所示。

    **图 3**  登录界面<a name="fig15272194819563"></a>  
    

    ![](figures/zh-cn_image_0233687050.png)

    输入对应ubuntu用户正确密码即可登录ADKInstaller（默认语言为英文，可以手动选择中文），如果使用root用户启动或密码输入错误，则无法启动软件。

    启动成功后界面如[图4](#fig83075490273)所示。可以看到ADKInstaller工具有三大功能：制卡、连接开发板、安装MindStudio。默认启动时为制卡界面。

    **图 4**  软件主页面<a name="fig83075490273"></a>  
    

    ![](figures/zh-cn_image_0233687020.png)

3.  制卡

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >如果你已经拥有一张制作好的SD卡。请确认开发板是否可以正常启动只需要升级，如果只需要升级可以点击“Skip”按钮跳过该步骤，直接在后续步骤中对开发板进行升级。  

    1.  登录后默认在制卡界面下，如果有切换到其他界面，则点击按钮“STEP 01 Make SD Card”，进入制卡界面。

        首先，如[图5](#fig2122418112812)查看软件许可，确认“OK”后才可以进行后续操作。

        **图 5**  制卡软件许可<a name="fig2122418112812"></a>  
        

        ![](figures/zh-cn_image_0233687065.png)

    2.  如[图6](#fig9827841133717)需要在**Host components**下拉框中选择需要的源，然后选择制卡界面右下方的“Make SD”。工具会自动下载环境依赖，安装依赖包准备制卡环境。

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >-   若当前ubuntu在使用ADKInstaller工具之前未执行过apt-get update 命令，建议先手动在命令行下执行apt-get update命令，否则apt依赖可能会安装不成功。 &emsp;参见FAQ[https://gitee.com/lovingascend/ADKInstaller/blob/master/FAQ.md](https://gitee.com/lovingascend/ADKInstaller/blob/master/FAQ.md)
    >-   国外用户建议选择默认源，即：use the default apt-get source
    >-   国内用户建议选择清华源，即：use the https://mirrors.tuna.tsinghua.edu.cn/ubuntu/apt-get source

    其中<u>**SD INFO**</u>在鼠标悬停时可以查看到选择的制卡开发板的USB网口IP和网口ip。未选择时默认USB网口ip为192.168.158.2，默认网口ip为192.168.157.2。

    **图 6**  制卡开始<a name="fig9827841133717"></a>  
        

    ![](figures/zh-cn_image_0233687173.png)

    3.  点击 ”Make SD“按钮后，会弹出[图7](#fig1595221213352)所示的界面。

        **图 7**  制卡选择<a name="fig1595221213352"></a>  
        

        ![](figures/zh-cn_image_0233687180.png)

        a.将带有SD卡的读卡器插入PC机，点击“Refresh”按钮，选择SD卡对应的名称。

        b.选择制卡后的开发者板USB网卡IP（默认为192.168.158.2）、物理网卡IP，此时物理网卡IP会与USB网卡IP绑定，后续物理网卡IP地址会随USB网卡IP地址自动更新。

        c.单击“OK”，工具开始制作SD卡。

    4.  制卡成功后，弹出如[图8](#fig051214619365)所示弹窗。点击“OK”即可。

        **图 8**  制卡成功<a name="fig051214619365"></a>  
        ![](figures/制卡成功.png "制卡成功")

4.  连接开发板

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >如果开发板已经连接过，可以点击“Skip”跳过当前步骤，直接进行下一步。  

    将制作完成的SD卡插入Atlas 200 DK，用type-c线连接PC端USB端口和Atlas 200 DK的type-c接口。给单板上电，等待约15分钟后，单板4个led灯全亮，说明单板正常启动。

    如选择制卡界面左侧的“STEP 02 Connect Atlas 200 DK”按钮。刷新网络（点击 “Refresh”按钮），会自动选择虚拟网卡，如果有多块开发板请选择对应的虚拟网卡。选择对应的Atlas 200 DK单板IP。点击“Connect”连接单板。

    ![](figures/zh-cn_image_0233687197.png)

    如果对开发板连接有疑问，可以点击**tips**按钮，查看连接的详细说明。

5.  安装Mindstudio

    1.  如[图9](#fig1943017118568)选择功能界面的“STEP 03 Setup Mind Studio”按钮，进入IDE安装界面。查看软件许可，确认“OK”。

        **图 9**  安装Mindstudio软件许可<a name="fig1943017118568"></a>  
        

        ![](figures/zh-cn_image_0233687203.png)

    2.  如[图10](#fig13380547155712)在**apt-get install、pip install、pip3 install**三个下拉框中选择需要的源，然后点击界面右下方的“Setup”，工具自动下载并安装环境依赖，准备MindStudio安装环境。

        **图 10**  Mindstudio安装准备<a name="fig13380547155712"></a>  
        

        ![](figures/zh-cn_image_0233687204.png)

    3.  如[图11](#fig591311510594)，当依赖安装好，工具提示开始安装MindStudio。用户选择“OK”，开始安装MindStudio工具。

        **图 11**  Mindstudio开始安装弹窗<a name="fig591311510594"></a>  
        ![](figures/Mindstudio开始安装弹窗.png "Mindstudio开始安装弹窗")

    4.  如[图12](#fig196509151219)，MindStudio安装完成。

        **图 12**  Mindstudio安装完成弹窗<a name="fig196509151219"></a>  
        ![](figures/Mindstudio安装完成弹窗.png "Mindstudio安装完成弹窗")

    5.  确认“OK”后，MindStudio会启动，然后的操作需要在Mindstudio中完成；启动时会询问是否导入配置，请参考完成后面步骤。

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >这里ADKInstaller的使命已经完成了，可以在root用户下执行  **./del\_sudo.sh  _username_**  删除本次专门为当前用户开启的sudo权限。  

6.  配置DDK/开发板
    1.  配置DDK

        请参考如下链接完成DDK配置。

        [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200347960.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/zh-cn_topic_0200347960.html)

    2.  配置开发板

        请参考如下链接，完成开发板配置，添加你刚才已经链接上的开发板。

        [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200347922.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/zh-cn_topic_0200347922.html)

    3.  开发板升级

        如果你的开发板版本较低，请参考如下链接完成开发板升级。

        [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200348044.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/zh-cn_topic_0200348044.html)

        如果升级失败，请参考如下链接用手工方式升级。

        [https://www.huaweicloud.com/ascend/doc/Atlas200DK/1.31.0.0\(beta\)/zh/zh-cn\_topic\_0182634979.html](https://www.huaweicloud.com/ascend/doc/Atlas200DK/1.31.0.0(beta)/zh/zh-cn_topic_0182634979.html)

    4.  同步lib库

        [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0201537270.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/zh-cn_topic_0201537270.html)



## 运行首个AI应用<a name="section147911829155918"></a>

请参考如下链接完成首个AI应用的运行。

[https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200347819.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/zh-cn_topic_0200347819.html)

## 相关说明<a name="section1676879104"></a>

-   **源码方式运行ADKInstaller**
    1.  获取源码包

        将  [https://gitee.com/Atlas200DK/ADKInstaller](https://gitee.com/Atlas200DK/ADKInstaller)  仓中的代码下载至Ubuntu任意目录，例如代码存放路径为：$HOME/ADKInstaller。

    2.  安装此应用中所需要的环境

        安装python3、pyqt

        **sudo pip3 install --upgrade pip**

        **sudo pip3 install PyQt5 -i  [http://mirrors.aliyun.com/pypi/simple/](http://mirrors.aliyun.com/pypi/simple/)    --trusted-host  [mirrors.aliyun.com](http://mirrors.aliyun.com/)**

        **sudo apt-get install qt5-default qttools5-dev-tools**

        **pip3 install paramiko**

        **pip3 install psutil**

    3.  程序运行

        进入代码下载目录，例如：$HOME/ADKInstaller，执行以下命令启动ADKInstaller，

        **python3  ADKInstaller.py**

        会出现登录界面。


-   **参考资料**

    Atlas200DK文档参考：

    [https://www.huaweicloud.com/ascend/doc/Atlas200DK/1.31.0.0\(beta\)/zh/zh-cn\_topic\_0188535624.html](https://www.huaweicloud.com/ascend/doc/Atlas200DK/1.31.0.0(beta)/zh/zh-cn_topic_0188535624.html)

    MindStudio使用参考：

    [https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0\(beta\)/zh/zh-cn\_topic\_0200347859.html](https://www.huaweicloud.com/ascend/doc/mindstudio/2.1.0(beta)/zh/zh-cn_topic_0200347859.html)

    Atlas200DK 论坛:

    [https://bbs.huaweicloud.com/forum/forum-726-1.html](https://bbs.huaweicloud.com/forum/forum-726-1.html)

## 常见问题

 &emsp;参见[https://gitee.com/lovingascend/ADKInstaller/blob/master/FAQ.md](https://gitee.com/lovingascend/ADKInstaller/blob/master/FAQ.md)


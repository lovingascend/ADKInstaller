## 常见问题
### 1、

#### 【问题描述】

&emsp;新安装的ubuntu虚拟机环境，直接下载使用ADKInstaller工具，apt-get update使用的是非默认源，发现总是安装依赖失败。

#### 【问题解决】

&emsp;（1） 新环境直接使用，需要先使用默认源进行更新，或者手动在终端进行apt-get update后再使用ADKInstaller工具进行更新。

<br>&emsp;（2） 海外用户应使用默认源的方式安装依赖，其他源方式更适用于国内，其他源根据实际情况自行选择。

### 2、

#### 【问题描述】

&emsp;系统的pip3无法使用，导致ADKInstaller工具安装依赖失败。

#### 【问题解决】

&emsp;root用户下使用pip3 --version系统报错pip3二进制文件异常，需要卸载并重装pip3，使用命令

**sudo python3 –m pip uninstall pip && sudo apt install python3-pip --reinstall**

### 3、

#### 【问题描述】

&emsp;点击暂停按钮时，当前正在安装的依赖并没有停止，而是安装完当前的依赖后才会停止。

#### 【问题分析】

&emsp;由于在一些情况下，强行kill掉apt进程会导致apt被锁住，因此，这里工具并不会强行停止当前正在安装的依赖，而是把未安装的依赖进程停止掉。所以出现这种情况属于正常现象，用户等待当前依赖安装完成后再继续操作即可。

### 4、

#### 【问题描述】

&emsp;pip2的依赖无论怎么换源都安装不成功

#### 【问题解决】

&emsp;尝试关闭并重新打开后安装成功。

### 5、

#### 【问题描述】

&emsp;(1)使用ADK工具，已经执行过add_sudo.sh脚本，但是运行时还是报错 do you add sudo for the current user？
&emsp;(2)./add_sudo.sh username 时提示错误:please check and fix the error line of the /etc/sudoers.d/xxx_specific !

#### 【问题解决】

&emsp;ADK工具会在/etc/sudoers.d/目录下生成ascend_specific_for_ADKInstaller文件，而这个目录下原本就有ascend_specific文件，或者其它文件，导致识别有问题，该目录下只能存在一个当前用户的specific文件。


rm -rf ADKInstaller
rm -rf ADKInstaller.0.0.1.tar.gz
rm -rf ADKInstall.spec
rm -rf build
pyinstaller -F -w ./src/ADKInstaller.py
cp ./src/*.gif ./dist/
cp ./src/*.sh ./dist/
cp -r ./src/html ./dist/
cp -r ./src/LanguageFiles ./dist/
cp ./src/config.ini ./dist/
rm ./src/passwd_manager.pickle
cp ./src/*.pickle ./dist/
mv ./dist/ ./ADKInstaller
tar -czf ADKInstaller.0.0.1.tar.gz ./ADKInstaller
